/* FeCommon.js  */
var feUI = feUI || {};
;(function(feUI, $, window, document, undefined) {
	'use strict';
	// Common variable
	var $window = $(window),
		$document = $(document),
		$html = ( !$.browser.mobile )
			? $('html').addClass('activeJS disableOutline')
			: $('html').addClass('activeJS disableOutline isMobile'),
		$body = $document.find('body').on({
			'keydown': function(e) {
				if ( e.keyCode === 9 ) $html.removeClass('disableOutline');
			},
			'keyup': function(e) {
				if ( e.keyCode === 13 ) $html.removeClass('disableOutline');
			},
			'click': function() {
				$html.addClass('disableOutline');
			}
		}),
		$wrap = $body.find('#wrap')
			.append('<div class="dimmedLayer"></div>'),
		$header = $wrap.find('#header'),
		$gnb = $header.find('#gnb'),
		$content = $body.find('#content'),
		$footer = $body.find('#footer'),
		$dimmedLayer = $wrap.find('.dimmedLayer'),
		winWidth = $window.width(),
		winHeight = $window.height(),
		scrollTopPos = $window.scrollTop(),
		headerHeightF = $header.outerHeight(),
		gnbHeight = $gnb.height(),
		activeClass = 'active',
		checkedClass = 'checked',
		currentClass = 'current',
		wrapMinWidth = parseInt($wrap.css('minWidth')),
		wideView = ( winWidth > wrapMinWidth ) ? true : false,
		fixedLayout = false,
		zoomMode = false,
		dimmedOpacity = 0.5,
		aniSpeed = 100,
		isMobView = $html.is('[class*=MobView]'),
		ieVer, gnbLayerMax, templateHTML;
	$dimmedLayer.css('opacity', dimmedOpacity);
	// jQuery UI Datepicker default setting
	$.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd',
		prevText: '이전 달',
		nextText: '다음 달',
		dayNames: ['일요일', '월요일', '화요일',
			'수요일', '목요일', '금요일', '토요일'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		monthNames: ['01', '02', '03', '04', '05',
			'06', '07', '08', '09', '10', '11', '12'],
		yearSuffix: '',
		showOtherMonths: true,
		showMonthAfterYear: true
	});
	feUI.datepickerScope = function(captionVal) {
		var $datepicker = $document.find('.ui-datepicker-calendar'),
			captionTxt = ( captionVal ) ? captionVal : '달력';
		$datepicker.prepend('<caption>' + captionTxt + '</caption>')
			.find('th').attr('scope', 'col');
	};
	// Old IE
	feUI.setOldIE = function() {
		var usrAgent = window.navigator.userAgent,
			msieCheck = usrAgent.match(/MSIE (\d+)/),
			tridentCheck = usrAgent.match(/Trident\/(\d+)/),
			updateURL = 'http://windows.microsoft.com/ko-kr/internet-explorer/download-ie',
			ieInfo = {
				isIE: false,
				trueVer: 0,
				activeVer: 0,
				cpMode: false
			};
		if ( tridentCheck ) { // Check gt IE7
			ieInfo.isIE = true;
			ieVer = ieInfo.trueVer = parseInt(tridentCheck[1], 10) + 4;
		}
		if ( msieCheck ) { // Check lt IE8
			ieInfo.isIE = true;
			ieVer = ieInfo.activeVer = parseInt(msieCheck[1]);
		} else {
			ieInfo.activeVer = ieInfo.trueVer;
		}
		// IE Compatibility Mode(IE8 to IE7)
		if ( ieInfo.isIE && ieInfo.activeVer < 8
			&& ieInfo.trueVer < 9 ) {
			ieInfo.cpMode = ieInfo.trueVer !== ieInfo.activeVer;
		}
		if ( ieInfo.isIE && ieInfo.activeVer < 7 ) window.location = updateURL;
		if ( ieInfo.isIE ) {
			$html.addClass('ie' + ieInfo.activeVer + 'Only');
			if ( ieInfo.trueVer < 9 ) $html.addClass('ie' + ieInfo.trueVer + 'Origin');
		}
		if ( !ieInfo.isIE || ieInfo.activeVer > 8 ) $html.addClass('mdBrowser');
		( ieInfo.cpMode ) ? $html.addClass('cpMode') : $html.removeClass('cpMode');
		return ieInfo;
	};
	feUI.setOldIE();
	// Old IE
	feUI.oldBlowser = function() {
		var usrAgent = window.navigator.userAgent,
			msieCheck = usrAgent.match(/MSIE (\d+)/),
			tridentCheck = usrAgent.match(/Trident\/(\d+)/),
			updateURL = 'http://windows.microsoft.com/ko-kr/internet-explorer/download-ie',
			ieInfo = {
				isIE: false,
				trueVer: 0,
				activeVer: 0,
				cpMode: false
			};
		if ( tridentCheck ) { // Check gt IE7
			ieInfo.isIE = true;
			ieVer = ieInfo.trueVer = parseInt(tridentCheck[1], 10) + 4;
		}
		if ( msieCheck ) { // Check lt IE8
			ieInfo.isIE = true;
			ieVer = ieInfo.activeVer = parseInt(msieCheck[1]);
		} else {
			ieInfo.activeVer = ieInfo.trueVer;
		}
		// IE Compatibility Mode(IE8 to IE7)
		if ( ieInfo.isIE && ieInfo.activeVer < 8
			&& ieInfo.trueVer < 9 ) {
			ieInfo.cpMode = ieInfo.trueVer !== ieInfo.activeVer;
		}
		if ( ieInfo.isIE && ieInfo.activeVer < 7 ) window.location = updateURL;
		if ( ieInfo.isIE ) {
			$html.addClass('ie' + ieInfo.activeVer + 'Only');
			if ( ieInfo.trueVer < 9 ) $html.addClass('ie' + ieInfo.trueVer + 'Origin');
		}
		if ( !ieInfo.isIE || ieInfo.activeVer > 8 ) $html.addClass('mdBrowser');
		( ieInfo.cpMode ) ? $html.addClass('cpMode') : $html.removeClass('cpMode');
		return ieInfo;
	};
	feUI.oldBlowser();
	// Skip To Content
	feUI.skipToContent = function() {
		var $skipNaviBtn = $body.find('a.skipToContent'),
			tgId = $skipNaviBtn.attr('href');
		if ( !$skipNaviBtn.length ) return false;
		if ( !$content.length ) $skipNaviBtn.hide();
		$skipNaviBtn.on('click', function(e) {
			e.preventDefault();
			$(tgId).attr('tabindex', 0).focus().on('keydown', function(e) {
				if ( e.keyCode === 9 ) $(this).removeAttr('tabindex');
			});
		});
	};
	feUI.skipToContent();
	// Cookie
	feUI.createCookie = function(name, value, days) {
		var expires;
		if ( days ) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = '; expires=' + date.toGMTString();
		} else expires = '';
		document.cookie = name + '=' + value + expires + '; path=/';
	};
	feUI.getCookie = function(name) {
		var nameEQ = name + '=';
		var ca = document.cookie.split(';');
		for ( var i=0; i < ca.length; i++ ) {
			var c = ca[i];
			while ( c.charAt(0) == ' ' ) c = c.substring(1, c.length);
			if ( c.indexOf(nameEQ) == 0 ) return c.substring(nameEQ.length, c.length);
		}
		return null;
	};
	feUI.deleteCookie = function(name) {
		feUI.createCookie(name, '', -1);
	};
	// Placeholder
	feUI.placeholder = function() {
		var $placeholderInp = $('[placeholder]'),
			placeholderClass = 'placeholderTxt',
			phSupported = document.createElement('input').placeholder !== undefined;
		if ( phSupported ) return false;
		if ( ieVer < 10 ) {
			$placeholderInp.each(function() {
				var $this = $(this),
					type = $this.attr('type'),
					phTxt = $this.attr('placeholder');
				if ( ieVer > 8 && type === 'password' ) $this.attr('type', 'text');
				if ( $this.val() === '' ) $this.addClass(placeholderClass).val(phTxt);
				$this.on({
					'focus': function() {
						if ( ieVer > 8 && type === 'password' )
							$this.attr('type', 'password');
						if ( $this.val() === phTxt )
							$this.val('').removeClass(placeholderClass);
					},
					'focusout': function() {
						if ( $this.val() === '' ) {
							$this.val('').addClass(placeholderClass);
							( ieVer > 8 && type === 'password' )
								? $this.attr('type', 'text').val(phTxt)
								: $this.val(phTxt);
						}
					}
				});
			});
		}
	};
	feUI.placeholder();
	feUI.focusRotation = function(evtTg, tgWrap, closeBtn) {
		var $tgWrap = tgWrap,
			$firstFocusTg = $tgWrap.find(':focusable:first'),
			$lastFocusTg = $tgWrap.find(':focusable:last');
		$tgWrap.attr('tabindex', 0).focus().on('keydown', function(e) {
			var $this = $(this);
			if ( !$(e.target).is(tgWrap) ) return;
			if ( e.keyCode === 9 && e.shiftKey ) {
				e.preventDefault();
				$this.removeAttr('tabindex');
				( closeBtn ) ? closeBtn.focus() : $lastFocusTg.focus();
			} else if ( e.keyCode === 9 && !e.shiftKey ) {
				e.preventDefault();
				$this.removeAttr('tabindex');
				$firstFocusTg.focus();
			}
		});
		$firstFocusTg.off('keydown').on('keydown', function(e) {
			if ( e.keyCode === 9 && e.shiftKey ) {
				e.preventDefault();
				( closeBtn ) ? closeBtn.focus() : $lastFocusTg.focus();
			}
			if ( ( e.keyCode === 9 && !e.shiftKey )
				&& ( $firstFocusTg.is($lastFocusTg) ) ) {
				e.preventDefault();
			}
		});
		$lastFocusTg.off('keydown').on('keydown', function(e) {
			if ( e.keyCode === 9 && !e.shiftKey ) {
				e.preventDefault();
				( closeBtn ) ? closeBtn.focus() : $firstFocusTg.focus();
			}
			if ( ( e.keyCode === 9 && e.shiftKey )
				&& ( $firstFocusTg.is($lastFocusTg) ) ) {
				e.preventDefault();
			}
		});
	};
	// feForm
	feUI.setFormEl = function() {
		var $formEl = $wrap.find(':radio, :checkbox, :file'),
			exFilter = $('.hideEl, .originType, .rsvTime, .fileTxt :file');
		if ( !$formEl.length ) return false;
		$formEl.not(exFilter).feForm();
		//feUI.cssPseudoEl();
	};
	feUI.setFormEl();

	$('select').feForm();

	// mainSlideNum
	feUI.tupperSlide = function(){
		$('.mainSlider').bxSlider({
			minSlides: 2,
			maxSlides: 2,
			moveSlides: 1,
			slideWidth: 1920,
			slideMargin: 0,
			pager: true,
			auto: true, //자동스크롤 여부
			autoControls:false,  //star,stop
			controls: false //next,prve
		});
		$('.prodSlider').bxSlider({
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 1,
			slideWidth: 640,
			slideMargin: 0,
			pager: true,
			auto: false, //자동스크롤 여부
			autoControls:false,  //star,stop
			controls: true, //next,prve
			pagerCustom: '.prod-pager'
        });
        $('.jsCataSlider').bxSlider({
            controls: true,
        });
        $('.jsBrandSlider').bxSlider({
            auto: true,
            nextSelector: '.brand-slider-ctrl .next',
            prevSelector: '.brand-slider-ctrl .prev',
            nextText: '다음 슬라이드 보기',
            prevText: '이전 슬라이드 보기',
            adaptiveHeight: true,
        });
        $('.jsCookingSlider').bxSlider({
            auto: true,
            pagerCustom: '.cooking-slider-pager',
            nextSelector: '.cooking-slider-ctrl .next',
            prevSelector: '.cooking-slider-ctrl .prev',
            nextText: '다음 슬라이드 보기',
            prevText: '이전 슬라이드 보기',
            adaptiveHeight: true,
        });
	};
	feUI.tupperSlide();
	// feFAQ
	feUI.setFAQList = function() {
		var $faqList = $wrap.find('.accordion');
		if ( !$faqList.length ) return false;
		$faqList.feFAQ();
	};
	feUI.setFAQList();
	// Default function
	feUI.defaultFuncSet = function() {
		feUI.setFormEl();
		feUI.contTab();
		feUI.setFAQList();
	};

	// feTab
	feUI.contTab = function() {
		var $tabs = $wrap.find('.popTab');
		$tabs.feTab();
	};
	feUI.contTab();

	feUI.setTab = function() {
		var tabList = $('.tabJs li');
		tabList.find('a').on('click',function(){
			var tabHref = $(this).attr('href');

			tabList.removeClass('current');
			$(this).parent().addClass('current');
			$('.popTabCont').hide();
			$('.tabCont').hide();
			$(tabHref).show();
			return false;
		});

		tabList.eq(0).find('a').click();
		$('.popTabCont').eq(0).show();
	};
	feUI.setTab();

	// modal
	feUI.modal = function() {
		var $modalBtn = $wrap.find('.modalBtn'),
			$modal = $wrap.find('.modal'),
			data = 'modal',
			$closeBtn = $modal.find('.closeBtn');
		if ( !$modal.length ) return false;
		$modalBtn.on('click', function(e) {
			var $this = $(this),
				dataClass = '.' + $(this).data(data),
				$tg = $modal.filter(dataClass);
			e.preventDefault();
			$('html,body').css('overflow','hidden');
			$this.addClass(currentClass);
			$tg.fadeIn(aniSpeed);
			$dimmedLayer.fadeIn(aniSpeed);
			feUI.focusRotation($this, $modal);
		});
		$closeBtn.on('click', function(e) {
			var $this = $(this),
				$tg = $this.closest($modal);
			e.preventDefault();
			$('html,body').css('overflow','auto');
			$tg.fadeOut(aniSpeed);
			$dimmedLayer.fadeOut(aniSpeed);
			$modalBtn.filter('.current').focus().removeClass(currentClass);
		});
	};
	feUI.modal();
	// Toggle Form
	feUI.toggleFormMethod = {
		tgEl: {
			hideClass: 'tgHideEl' // IE7
		},
		init: function() {
			var tgEl = feUI.toggleFormMethod.tgEl;
			tgEl.$toggleTrigger = $wrap.find(':checkbox[data-tg-trigger], \
				:radio[data-tg-trigger], select:has([data-tg-trigger])'),
			tgEl.$toggleWrap = $wrap.find('[data-tg-wrap]').attr({
				'role': 'region',
				'aria-expanded': false
			}),
			tgEl.$uncheckWrap = $wrap.find('[data-tg-uncheck]')
				.not('[data-tg-trigger]'),
			tgEl.$toggleGroup = $wrap.find('[data-tg-group]')
				.not('[data-tg-trigger]');
			if ( !tgEl.$toggleTrigger.length ) return false;
			tgEl.$toggleTrigger.on('change check', function() {
				var $this = ( $(this).prop('tagName') === 'INPUT' )
						? $(this) : $(this).find(':checked');
				feUI.toggleFormMethod._toggle($this);
			}).trigger('check');
		},
		_toggle: function($this) {
			var tgEl = feUI.toggleFormMethod.tgEl,
				tgTriggerVal = $this.attr('data-tg-trigger'),
				tgGroupVal = $this.attr('data-tg-group'),
				tgUncheckVal = $this.attr('data-tg-uncheck'),
				$tgWrap = tgEl.$toggleWrap
					.filter('[data-tg-wrap=' + tgTriggerVal + ']'),
				$tgUncheckWrap = tgEl.$uncheckWrap
					.filter('[data-tg-uncheck=' + tgUncheckVal + ']'),
				$tgGroup = tgEl.$toggleGroup
					.filter('[data-tg-group=' + tgGroupVal + ']'),
				checkedVal = $this.is(':checked');
			if ( checkedVal ) {
				if ( tgGroupVal ) $tgGroup.hide().addClass(tgEl.hideClass)
					.attr('aria-expanded', false);
				$tgWrap.show().removeClass(tgEl.hideClass)
					.attr('aria-expanded', true);
				$tgUncheckWrap.hide().addClass(tgEl.hideClass)
					.attr('aria-expanded', false);
			} else {
				$tgWrap.hide().addClass(tgEl.hideClass)
					.attr('aria-expanded', false);
				$tgUncheckWrap.show().removeClass(tgEl.hideClass)
					.attr('aria-expanded', true);
			}
		}
	};
	feUI.toggleFormMethod.init();

	feUI.consoleBoxShow = function() {
		$('.listItem > a').on('mouseenter', function(e) {
			$('.consoleBox').hide();
			$(this).next('.consoleBox').hide().show();
			e.preventDefault();
		});
		$('.listItem').on('mouseleave', function(e) {
			$('.consoleBox').hide();
			e.preventDefault();
		});
	}
	feUI.consoleBoxShow();
	feUI.toolTip = function() {
		$('.button-finalOrder > span').on('mouseenter',function(){
			$('.toolTip').fadeIn();
		});
		$('.toolTip > a').on('click',function(e){
			e.preventDefault();
			$(this).parent().fadeOut();
		});
	}
	feUI.toolTip();
	// datePiker
	feUI.calendar = function() {
	   $("#reservation-date01").datepicker({
	        showOn: "button",
	        buttonImage: "../asset/img/common/icon_date01.png", //버튼이미지에 사용할 이미지 경로
	        buttonImageOnly: true, //버튼이미지를 나오게 한다.
	        dateFormat: 'yy-mm-dd', //데이터 포멧형식
	        minDate: '-60M',     //오늘 부터 3달전까지만 선택 할 수 있다.
	        maxDate: '+36M',    //오늘 부터 36개월후까지만 선택 할 수 있다.
	        changeMonth: true,   //달별로 선택 할 수 있다.
	        changeYear: true,    //년별로 선택 할 수 있다.
	        showOtherMonths: true,  //이번달 달력안에 상/하 빈칸이 있을경우 전달/다음달 일로 채워준다.
	        selectOtherMonths: true,
	        prevText: "이전달",
	        nextText: "다음달",
	        closeText: "닫기",
	        currentText: "오늘",
	        showMonthAfterYear: true,
	        monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
	        monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
	        dayNames: ["일", "월", "화", "수", "목", "금", "토"],
	        dayNamesShort: ["일", "월", "화", "수", "목", "금", "토"],
	        dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
	        //numberOfMonths: 3,   //오늘부터 3달치의 달력을 보여준다.
	        showButtonPanel: false  //오늘 날짜로 돌아가는 버튼 및 닫기 버튼을 생성한다.
	    });

	   	 $("#reservation-date02").datepicker({
	        showOn: "button",
	        buttonImage: "../asset/img/common/icon_date01.png", //버튼이미지에 사용할 이미지 경로
	        buttonImageOnly: true, //버튼이미지를 나오게 한다.
	        dateFormat: 'yy-mm-dd', //데이터 포멧형식
	        minDate: '-60M',     //오늘 부터 3달전까지만 선택 할 수 있다.
	        maxDate: '+36M',    //오늘 부터 36개월후까지만 선택 할 수 있다.
	        changeMonth: true,   //달별로 선택 할 수 있다.
	        changeYear: true,    //년별로 선택 할 수 있다.
	        showOtherMonths: true,  //이번달 달력안에 상/하 빈칸이 있을경우 전달/다음달 일로 채워준다.
	        selectOtherMonths: true,
	        prevText: "이전달",
	        nextText: "다음달",
	        closeText: "닫기",
	        currentText: "오늘",
	        showMonthAfterYear: true,
	        monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
	        monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
	        dayNames: ["일", "월", "화", "수", "목", "금", "토"],
	        dayNamesShort: ["일", "월", "화", "수", "목", "금", "토"],
	        dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
	        //numberOfMonths: 3,   //오늘부터 3달치의 달력을 보여준다.
	        showButtonPanel: false  //오늘 날짜로 돌아가는 버튼 및 닫기 버튼을 생성한다.
	    });
	};
	feUI.calendar();
	// Window Event
	$window.on({
		'resize': function() {
			if ( winWidth !== $window.width() ) {
				winWidth = $window.width(),
				wideView = ( winWidth > wrapMinWidth ) ? true : false;
			}
			if ( winHeight !== $window.height() ) {
				winHeight = $window.height();
			}
		},
		'scroll': function() {
			scrollTopPos = $window.scrollTop();
		}
	});
})(feUI, jQuery, window, document);
